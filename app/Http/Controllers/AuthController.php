<?php

namespace App\Http\Controllers;


use App\Models\User;
use App\Services\AuthService;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    private $authService;

    public function __construct(AuthService $authService) {
        $this->authService = $authService;
    }

    public function index() {
        return User::all();
    }

    public function auth(Request $request) {
        $credential = $request->only(['username', 'password']);

        if(empty($user = $this->authService->validateCredential($credential['username'], $credential['password']))) {
            return response()->json([
                'error' => [
                    'message' => 'Invalid credential'
                ]
            ], 404);
        }

        $newSession = $this->authService->newSession($user);

        return [
            'data' => [
                'session_key' => $newSession->key,
                'user' => $user,
            ],
        ];
    }

    public function register(Request $request) {
        $credential = $request->only(['username', 'password']);

        $newUser = $this->authService->newUser($credential['username'], $credential['password']);

        if(empty($newUser)) {
            return response()->json([
                'error' => [
                    'message' => 'Register failed',
                ],
            ], 501);
        }

        return [
            'data' => [
                'user' => $newUser,
            ],
        ];
    }

    public function validateSession(Request $request) {
        if(empty($session = $this->authService->validateSession($request->get('session_key', null)))) {
            return response()->json([
                'error' => [
                    'message' => 'Invalid session key',
                ],
            ], 404);
        }

        return [
            'data' => [
                'session_key' => $session->key,
                'user' => $this->authService->getUser($session->user_id)
            ],
        ];
    }
}