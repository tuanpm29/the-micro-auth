<?php

namespace App\Services;


use App\Models\Session;
use App\Models\User;
use Illuminate\Support\Str;

class AuthService
{
    public function validateCredential($username, $password) {
        return User::where('username', $username)
                ->where('password', hash('sha256', $password))->first();
    }

    public function newSession($user) {
        do {
            $newKey = Str::random(64);
        } while(Session::where('key', $newKey)->count() > 0);

        $newSession = Session::create([
            'user_id' => $user->id,
            'key' => $newKey
        ]);

        return $newSession;
    }

    public function validateSession($sessionKey) {
        return Session::where('key', $sessionKey)->first();
    }

    public function getUser($userId) {
        return User::find($userId);
    }

    public function newUser($username, $password) {
        if(User::where('username', $username)->count() > 0) {
            return false;
        }

        return User::create([
            'username' => $username,
            'password' => hash('sha256', $password)
        ]);
    }
}